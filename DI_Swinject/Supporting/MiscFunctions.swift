//
//  MiscFunctions.swift
//  DI_test
//
//  Created by IT Resource Center on 10/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import UIKit

class Misc {
    static func stringGen(length:Int, charset:String) -> String{
        let chars = Array(charset)
        var string = ""
        while string.count != length {
            string.append(chars[Int.random(in: 0..<chars.count)])
        }
        return string
    }
    
    static func colorGen() -> UIColor{
        let r = CGFloat.random(in: 0...255) / 255
        let g = CGFloat.random(in: 0...255) / 255
        let b = CGFloat.random(in: 0...255) / 255
        let a = CGFloat.random(in: 0...100) / 100
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
}
