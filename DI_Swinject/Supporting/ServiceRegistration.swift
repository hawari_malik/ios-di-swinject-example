//
//  ServiceRegistration.swift
//  DI_test
//
//  Created by IT Resource Center on 11/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import Swinject

extension Container {
    static let shared:Container = {
        let container = Container()
        
        //Using Original Swinject Service Registration
        container.register(ProfileModule.self) {_ in ProfileModule()}
        
        //Using SwinjectAutoRegistration 
        container.autoregister(LoginModule.self, initializer: LoginModule.init)
        container.autoregister(DashboardModule.self, initializer: DashboardModule.init)
        return container
    }()
}
