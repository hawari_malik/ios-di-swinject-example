//
//  LoginVMSameLetter.swift
//  DI_test
//
//  Created by IT Resource Center on 07/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation

class LoginVMSameFields : LoginVM {
    func validate(username u: String?, password p: String?) -> String? {
        let us = u ?? ""
        let ps = p ?? ""
        if us.count == 0 {
            return "username empty"
        }
        if ps.count == 0 {
            return "password empty"
        }
        if(us != ps){
            return "username != password"
        }
        return nil
    }
}
