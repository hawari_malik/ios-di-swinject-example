//
//  LoginModule.swift
//  DI_Swinject
//
//  Created by IT Resource Center on 11/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation

class LoginModule {
    func component() -> LoginVM {
        LoginVMBypass()
    }
}
