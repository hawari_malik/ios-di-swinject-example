//
//  ProfileVMEmpty.swift
//  DI_test
//
//  Created by IT Resource Center on 10/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import UIKit

class ProfileVMEmpty : ProfileVM {
    func getName() -> String {
        "-"
    }
    
    func getID() -> String {
        "-"
    }
    
    func getColor() -> UIColor {
        UIColor.black
    }
}
