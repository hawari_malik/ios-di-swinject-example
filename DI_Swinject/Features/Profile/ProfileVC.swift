//
//  ProfileVC.swift
//  DI_test
//
//  Created by IT Resource Center on 10/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration

class ProfileVC : UIViewController {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblId: UILabel!
    
    //Using SwinjectAutoRegistration Resolve Operator
    private var module: ProfileModule = Container.shared~>
    private lazy var vm: ProfileVM = module.component()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"
        imgProfile.backgroundColor = vm.getColor()
        lblName.text = vm.getName()
        lblId.text = vm.getID()
    }

}
