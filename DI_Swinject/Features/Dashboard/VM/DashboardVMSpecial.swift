//
//  DashboardVMSpecial.swift
//  DI_Swinject
//
//  Created by IT Resource Center on 24/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import UIKit

class DashboardVMSpecial : DashboardVM {
    let pvm : ProfileVM
    
    init(_ vm: ProfileVM){
        pvm = vm
    }
    
    func getColor() -> UIColor {
        return Misc.colorGen()
    }
    
    func getTexts() -> [String] {
        return ["\(pvm.getColor())", pvm.getID(), pvm.getName()]
    }
}
