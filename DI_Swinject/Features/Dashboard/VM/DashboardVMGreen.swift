//
//  DashboardVMGreen.swift
//  DI_test
//
//  Created by IT Resource Center on 07/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import UIKit

class DashboardVMGreen : DashboardVM {
    func getColor() -> UIColor {
        return UIColor.green
    }
    func getTexts() -> [String] {
        return [
        "the loading above is green colored",
        "this is the second vm",
        "please disregard the texts",
        "i simply don't know what to write"
        ]
    }
}
