//
//  DashboardVMRed.swift
//  DI_test
//
//  Created by IT Resource Center on 07/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import UIKit

class DashboardVMRed : DashboardVM {
    func getColor() -> UIColor {
        return UIColor.red
    }
    func getTexts() -> [String] {
        return [
        "the loading above is red colored",
        "the first vm available",
        "please ignore the texts",
        "i don't know what to write"
        ]
    }
}
