//
//  DashboardVM.swift
//  DI_test
//
//  Created by IT Resource Center on 07/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import UIKit

protocol DashboardVM{
    func getColor()->UIColor
    func getTexts()->[String]
}
